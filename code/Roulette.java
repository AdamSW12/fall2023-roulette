import java.util.Scanner;

public class Roulette {
    public final static Scanner in = new Scanner(System.in);
    public static int userMoney = 1000;
    public static RouletteWheel wheel = new RouletteWheel();

    public static void main(String[] args) {
        String play;
        System.out.println("Would you like to make a bet? (Y/N)");
        play = in.nextLine();
        play = play.toLowerCase();

        while((play.equals("y") || play.equals("yes"))){
            playGame();
            if(userMoney <= 0){
                break;
            }
            System.out.println("You have: " + userMoney + "$");
            System.out.println("Play again?");
            play = in.next();
        }

        System.out.println("thank you for playing! You end the game with: " + userMoney);
    }
    public static void playGame(){
        String userChoice;
        int userNum = 0;
        int userBet = 0;
        Boolean betOnNum;
                
       do{
            System.out.println("How much do you want to bet?");
            userBet = in.nextInt();
            if(userBet > userMoney){
                System.out.println(Colours.RED + "Cant bet more money than you already have" + Colours.RESET);
            }
       }while(userBet > userMoney);
       
       System.out.println("What do you want to bet on?");
       userChoice = in.next();

       if(isNum(userChoice)){
            userNum = Integer.parseInt(userChoice);

             while(userNum < 0 || userNum > 36){
                System.out.println(Colours.RED + "Number must be between 0 and 36" + Colours.RESET);
                userNum = in.nextInt();
            }
            betOnNum = true;
            System.out.println("You bet " + userBet + " on number: " + userNum);
       }
       else{
            System.out.println("You bet " + userBet + " on: " + userChoice);
            betOnNum = false;
       }
       if(betOnNum){
            numberBet(userNum, userBet);
       }
       else{
            userChoice = userChoice.toLowerCase();

            if(userChoice.contains("odd")){
                oddEvenBet(1, userBet);
            } else if (userChoice.contains("even")) {
                oddEvenBet(0, userBet);
            } else {
                colourBet(userBet, userChoice);
            }
       }

    }
    private static void numberBet(int userNum,int userBet){
        wheel.spin();
        int wheelNum = wheel.getValue();
        System.out.println("Wheel spin is: " + wheelNum);

        if(userNum == wheelNum){
            userMoney = userMoney + userBet * 35;
            System.out.println("You win! ");
            System.out.println("You now have " + userMoney + "$");
        }
        else{
            userMoney = userMoney - userBet;
            System.out.println("You lose!");
            System.out.println("You now have " + userMoney + "$");
        }
    }
    
    private static void oddEvenBet(int oddEven, int userBet){
        wheel.spin();
        int wheelNum = wheel.getValue();
        System.out.println("Wheel spin is: " + wheelNum);

        if(wheelNum % 2 == oddEven && wheelNum != 0){
            userMoney = userMoney + userBet;
            System.out.println("You win!");
            System.out.println("You now have " + userMoney + "$");
        }
        else{
            userMoney = userMoney - userBet;
            System.out.println("You lose!");
            System.out.println("You now have " + userMoney + "$");
        }
    }

    private static void colourBet(int userBet, String userChoice){
        wheel.spin();
        int wheelNum = wheel.getValue();
        String wheelColour = " ";

        if((wheelNum > 0 && wheelNum <= 10) || (wheelNum >= 19 && wheelNum <= 28 )){
            if(wheelNum % 2 == 0){
                wheelColour = "black";
            }
            else{
                wheelColour = "red";
            }
        }
        else if((wheelNum >= 11 && wheelNum <= 18) || (wheelNum >= 29 && wheelNum <= 36 )){
            if(wheelNum % 2 == 0){
                wheelColour = "red";
            }
            else{
                wheelColour = "black";
            }
        }

        System.out.println("The wheel spin is " + wheelNum);
        System.out.println(wheelNum + " is " + wheelColour);

        if(userChoice.equals(wheelColour) && wheelNum != 0){
            userMoney = userMoney + userBet;
            System.out.println("You win!");
            System.out.println("You now have " + userMoney + "$");   
        }
        else{
            userMoney = userMoney - userBet;
            System.out.println("You lose!");
            System.out.println("You now have " + userMoney + "$");
        }

    }

    private static boolean isNum(String userChoice) {
        try {
            Integer.parseInt(userChoice);
            return true;
        } catch (NumberFormatException efe) {
            return false;
        }
    }
    
}