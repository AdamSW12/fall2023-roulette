public enum Colours {
    RESET("\033[0m"),
    RED("\033[31m");

    private String colour;

    private Colours(String colour){
        this.colour=colour;
    }
    public String toString(){
        return this.colour;
    }
}
