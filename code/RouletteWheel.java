import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int number;

    public RouletteWheel(){
        random = new Random();
        number = 0;
    }

    public void spin(){
        number = random.nextInt(37);
    }

    public int getValue(){
        return number;
    }

}
